CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Features
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------
The Views Load All module is a simple pager plugins for view.
Inherit some features from following two modules.
* Views Load More (https://www.drupal.org/project/views_load_more)
* Views Show More module (https://www.drupal.org/project/views_show_more)
Site Admin can manage records count for 1st page as well as second(Last) Page.
Like, Site Admin can set 6 records for 1st page. And 10 or All remaining records
for second(Last) Page.

User can see only two pages, First to show initial record and Second(Last) to 
show remaining records.

FEATURES
--------
* Works both views ajax and no-ajax mode (Best for ajax mode).
* Option for result display method. Can choose Append or Replace method.
  (Inherit from Views Show More module)
* Option to override 1st page initial record count as well as second(Last) Page.
* Basic and advance animation system for result impression for ajax mode.

REQUIREMENTS
------------
 * Views (http://drupal.org/project/views)

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 Go to Administration » Structure » Views:

   * Create a new view or edit an existing one.
   * Add or select any display type like page, block etc.
   * In pager settings section click pager chooser link to change pager type.
   * Select Load All Pager and click apply.
   * In settings page enter the initial item count, Next items
     count, pager text etc.
   * Save pager settings.
   * Enable ajax mode and save the view.
   * Now check the view in front end and you will get your desire output.
   * Load All Pager system will not work in views live preview.

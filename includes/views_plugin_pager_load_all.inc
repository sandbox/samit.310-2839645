<?php

// @codingStandardsIgnoreFile
/**
 * @file
 * Extend Full Pager plugin to create show more pager.
 */
class ViewsLoadAllPlugin extends views_plugin_pager_full {

  /**
   * Summary title overwrite.
   */
  function summary_title() {
    $initial_page_items = !empty($this->options['initial_page_items']) ? $this->options['initial_page_items'] : $this->options['items_per_page'];

    $offset = '';
    if (!empty($this->options['offset'])) {
      $offset = ', '. t('skip ') . $this->options['offset'];
    }

    return format_plural(
            $initial_page_items, 'Initial page @initial_page_items items, ', 'Initial page @initial_page_items items, ', array('@initial_page_items' => $initial_page_items))
        . format_plural(
            $this->options['items_per_page'], 'Second page @count items', 'Second page @count items', array(
          '@count' => $this->options['items_per_page'],
            )
        ) . $offset;
  }

  /**
   * Options definition overwrite.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['result_display_method'] = array(
      'default' => 'append',
    );
    $options['load_all_text'] = array(
      'default' => t('Load All'),
      'translatable' => TRUE,
    );
    $options['initial_page_items'] = array(
      'default' => 0,
    );
    $options['items_per_page'] = array('default' => 'All');
    $options['effects'] = array(
      'contains' => array(
        'type' => array('default' => 'none'),
        'speed_type' => array('default' => ''),
        'speed' => array('default' => ''),
        'speed_value' => array('default' => ''),
      ),
    );
    return $options;
  }

  /**
   * Options form overwrite.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $exclude = array('total_pages', 'quantity', 'expose', 'tags');
    foreach ($exclude as $ex) {
      unset($form[$ex]['#title']);
      unset($form[$ex]['#description']);
      $form[$ex]['#attributes'] = array('class' => array('element-invisible'));
    }

    // Result display method.
    $form['result_display_method'] = array(
      '#type' => 'select',
      '#title' => t('Result display method'),
      '#description' => t('<strong>Append</strong> result display method append the new content after the existing content on the page in ajax mode and in no-ajax mode replace the content by page refresh. <strong>Replace</strong> result display method replace the content with new content both in ajax and no-ajax mode. In no-ajax mode it refresh the page.'),
      '#options' => array(
        'append' => t('Append'),
        'html' => t('Replace'),
      ),
      '#default_value' => $this->options['result_display_method'] ? $this->options['result_display_method'] : 'append',
      '#weight' => 0,
    );

    // Option for users to specify the text used on the 'show more' button.
    $form['load_all_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Load all pager link text'),
      '#description' => t('Text for the button which used to load more items. Like "Load All".'),
      '#default_value' => $this->options['load_all_text'] ? $this->options['load_all_text'] : t('Load all'),
      '#weight' => 1,
    );

    // Initial items count.
    $form['initial_page_items'] = array(
      '#type' => 'textfield',
      '#title' => t('Initial page items'),
      '#description' => t('The number of items to display initially. Initial page items must be 1 or greater than 1.'),
      '#default_value' => $this->options['initial_page_items'] ? $this->options['initial_page_items'] : 1,
      '#weight' => 3,
      '#element_validate' => array('views_load_all_initial_page_items_integer_validation'),
    );

    // Twick item per page description and weight.
    $form['items_per_page']['#title'] = t('Second page items');
    $form['items_per_page']['#default_value'] = $this->options['items_per_page'] ? $this->options['items_per_page'] : 'All';
    $form['items_per_page']['#description'] = t("Total number of items to display at Load all(Pager) click. Enter \"<strong>All</strong>\" to load All records.");
    $form['items_per_page']['#weight'] = 4;

    // Twick offset weight.
    $form['offset']['#weight'] = 5;

    // Twick pager id weight.
    $form['id']['#weight'] = 5;

    // Effects for loading adds new rows.
    $form['effects'] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
      '#title' => t('Animation'),
      '#input' => TRUE,
      '#weight' => 7,
    );
    $form['effects']['type'] = array(
      '#type' => 'select',
      '#options' => array(
        'none' => t('None'),
        'fade' => t('FadeIn'),
        'scroll' => t('Scroll to New Content'),
        'scroll_fade' => t('Scroll to New Content & FadeIn'),
      ),
      '#default_vaue' => 'none',
      '#title' => t('Animation Type'),
      '#default_value' => $this->options['effects']['type'],
    );
    $form['effects']['speed_type'] = array(
      '#type' => 'select',
      '#options' => array(
        'slow' => t('Slow'),
        'fast' => t('Fast'),
        'custom' => t('Custom'),
      ),
      '#states' => array(
        'visible' => array(
          '#edit-pager-options-effects-type' => array(
            array('value' => 'fade'),
            array('value' => 'scroll'),
            array('value' => 'scroll_fade'),
          ),
        ),
      ),
      '#title' => t('Animation Speed'),
      '#default_value' => $this->options['effects']['speed_type'],
    );
    $form['effects']['speed_value'] = array(
      '#type' => 'textfield',
      '#title' => t('Animation speed in ms'),
      '#states' => array(
        'visible' => array(
          '#edit-pager-options-effects-speed-type' => array('value' => 'custom'),
        ),
      ),
      '#default_value' => $this->options['effects']['speed_value'],
      '#element_validate' => array('element_validate_integer_positive'),
    );
  }

  /**
   * Options form validate.
   */
  function options_validate(&$form, &$form_state) {
    $effect_speed = $form_state['values']['pager_options']['effects']['speed_type'];
    $effect_speed_val = $form_state['values']['pager_options']['effects']['speed_value'];
    if ($effect_speed == 'custom') {
      if ($effect_speed_val == '') {
        form_set_error('pager_options][effects][speed_value', t('Animation speed is required.'));
      }
      else {
        $form_state['values']['pager_options']['effects']['speed'] = $effect_speed_val;
      }
    }
    else {
      $form_state['values']['pager_options']['effects']['speed'] = $effect_speed;
    }
  }

  /**
   * Query overwrite.
   */
  function query() {
    parent::query();

    $others_page = $this->options['items_per_page'];
    $limit = !empty($this->options['initial_page_items']) ? $this->options['initial_page_items'] : $others_page;
    $offset = !empty($this->options['offset']) ? $this->options['offset'] : 0;

    if ($this->current_page != 0) {
      $offset = $limit + (($this->current_page - 1) * $others_page) + $offset;
      if (intval($others_page)) {
        $limit = $others_page;
      }
      elseif ($others_page == 'All') {
        $limit = 0;
      }
    }

    $this->view->query->set_limit($limit);
    $this->view->query->set_offset($offset);
  }

  /**
   * Render overwrite.
   */
  function render($input) {
    if ($this->current_page == 0) {
      $pager_theme = views_theme_functions('pager_load_all', $this->view, $this->display);
      $output = theme($pager_theme, array(
        'parameters' => $input,
        'element' => $this->options['id'],
        'result_display_method' => $this->options['result_display_method'],
        'load_all_text' => $this->options['load_all_text']
      ));
      return $output;
    }
  }

  /**
   * Get pager total count.
   */
  function get_pager_total() {
    if ($items_per_page = intval($this->get_items_per_page())) {
      if ($initial_page_items = intval($this->get_initial_page_items())) {
        return 1 + ceil(($this->total_items - $initial_page_items) / $items_per_page);
      }
      else {
        return ceil($this->total_items / $items_per_page);
      }
    }
    else {
      return $this->total_items;
    }
  }

  /**
   * Execute the count query, which will be done just prior to the query
   * itself being executed.
   */
  function execute_count_query(&$count_query) {
    $this->total_items = $count_query->execute()->fetchField();
    if (!empty($this->options['offset'])) {
      $this->total_items -= $this->options['offset'];
    }

    $this->update_page_info();
    return $this->total_items;
  }

  /**
   * Get the page initial items count.
   */
  private function get_initial_page_items() {
    $items_per_page = intval($this->get_items_per_page());
    return isset($this->options['initial_page_items']) ? $this->options['initial_page_items'] : $items_per_page;
  }

}

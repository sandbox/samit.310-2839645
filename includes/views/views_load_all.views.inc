<?php

/**
 * @file
 * Views Pager plugin definition.
 */

/**
 * Implements hook_views_plugins().
 */
function views_load_all_views_plugins() {
  $plugins = array(
    'pager' => array(
      'load_all' => array(
        'title' => t('Load all pager'),
        'help' => t('Ajax based load all pager.'),
        'handler' => 'ViewsLoadAllPlugin',
        'help topic' => 'pager-load-all',
        'uses options' => TRUE,
        'parent' => 'full',
      ),
    ),
  );

  return $plugins;
}
